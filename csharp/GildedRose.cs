﻿using csharp.ItemClasses;
using System.Collections.Generic;

namespace csharp
{
    public class GildedRose
    {
        public List<RegularItem> Items;
        public GildedRose(List<Item> Items)
        {
            this.Items = new List<RegularItem>();
            Items.ForEach(item =>
            {
                if (item.Name.Contains("Aged Brie")) this.Items.Add(new AgedBrieItem(item));
                else if (item.Name.Contains("Sulfuras")) this.Items.Add(new SulfurasItem(item));
                else if (item.Name.Contains("Backstage passe")) this.Items.Add(new BackstagePassItem(item));
                else if (item.Name.Contains("Conjured")) this.Items.Add(new ConjuredItem(item));
                else this.Items.Add(new RegularItem(item));
            });
        }

        public void UpdateQuality()
        {
            Items.ForEach(item=>
            {
                item.UpdateItem();
            });
        }
    }
}
