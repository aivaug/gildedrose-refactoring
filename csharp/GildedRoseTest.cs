﻿using NUnit.Framework;
using System.Collections.Generic;

namespace csharp
{
    [TestFixture]
    public class GildedRoseTest
    {
        [Test]
        public void RegulaItemTest()
        {
            List<Item> Items = new List<Item> { 
                new Item { Name = "regular item", SellIn = 0, Quality = 0 }, //Regular item
                new Item { Name = "regular item", SellIn = 5, Quality = 20 } //Regular item
            };
            GildedRose app = new GildedRose(Items);
            for(int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            Assert.AreEqual(0, app.Items[0].Quality);
            Assert.AreEqual(-5, app.Items[0].SellIn);
            Assert.AreEqual(15, app.Items[1].Quality);
            Assert.AreEqual(0, app.Items[1].SellIn);
        }

        [Test]
        public void AgedBrieTest()
        {
            List<Item> Items = new List<Item> {
                new Item { Name = "Aged Brie 0", SellIn = 0, Quality = 0 }, //Aged Brie item
                new Item { Name = "Aged Brie 1", SellIn = 5, Quality = 20 } //Aged Brie item
            };
            GildedRose app = new GildedRose(Items);
            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            Assert.AreEqual(10, app.Items[0].Quality);
            Assert.AreEqual(-5, app.Items[0].SellIn);
            Assert.AreEqual(25, app.Items[1].Quality);
            Assert.AreEqual(0, app.Items[1].SellIn);
        }

        [Test]
        public void BackstagePassTest()
        {
            List<Item> Items = new List<Item> {
                new Item { Name = "Backstage passes 0", SellIn = 0, Quality = 0 }, //Backstage passes item
                new Item { Name = "ABackstage passes 1", SellIn = 5, Quality = 20 }, //Backstage passes item
                new Item { Name = "ABackstage passes 2", SellIn = 12, Quality = 20 }, //Backstage passes item
                new Item { Name = "ABackstage passes 3", SellIn = 7, Quality = 20 } //Backstage passes item
            };
            GildedRose app = new GildedRose(Items);
            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            Assert.AreEqual(0, app.Items[0].Quality);
            Assert.AreEqual(-5, app.Items[0].SellIn);
            Assert.AreEqual(35, app.Items[1].Quality);
            Assert.AreEqual(0, app.Items[1].SellIn);
            Assert.AreEqual(28, app.Items[2].Quality);
            Assert.AreEqual(7, app.Items[2].SellIn);
            Assert.AreEqual(33, app.Items[3].Quality);
            Assert.AreEqual(2, app.Items[3].SellIn);
        }

        [Test]
        public void SulfurasTest()
        {
            List<Item> Items = new List<Item> {
                new Item { Name = "Sulfuras 0", SellIn = 0, Quality = 0 }, //Sulfuras item
                new Item { Name = "Sulfuras 1", SellIn = 5, Quality = 80 }, //Sulfuras item
                new Item { Name = "Sulfuras 2", SellIn = 12, Quality = 80 }, //Sulfuras item
                new Item { Name = "Sulfuras 3", SellIn = 7, Quality = 25 } //Sulfuras item
            };
            GildedRose app = new GildedRose(Items);
            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            Assert.AreEqual(0, app.Items[0].Quality);
            Assert.AreEqual(0, app.Items[0].SellIn);
            Assert.AreEqual(80, app.Items[1].Quality);
            Assert.AreEqual(5, app.Items[1].SellIn);
            Assert.AreEqual(80, app.Items[2].Quality);
            Assert.AreEqual(12, app.Items[2].SellIn);
            Assert.AreEqual(25, app.Items[3].Quality);
            Assert.AreEqual(7, app.Items[3].SellIn);
        }

        [Test]
        public void ConjuredTest()
        {
            List<Item> Items = new List<Item> {
                new Item { Name = "Conjured 0", SellIn = 0, Quality = 0 }, //Conjured item
                new Item { Name = "Conjured 1", SellIn = 5, Quality = 40 }, //Conjured item
                new Item { Name = "Conjured 2", SellIn = 12, Quality = 30 }, //Conjured item
                new Item { Name = "Conjured 3", SellIn = 7, Quality = 45 } //Conjured item
            };
            GildedRose app = new GildedRose(Items);
            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            Assert.AreEqual(0, app.Items[0].Quality);
            Assert.AreEqual(-5, app.Items[0].SellIn);
            Assert.AreEqual(30, app.Items[1].Quality);
            Assert.AreEqual(0, app.Items[1].SellIn);
            Assert.AreEqual(20, app.Items[2].Quality);
            Assert.AreEqual(7, app.Items[2].SellIn);
            Assert.AreEqual(35, app.Items[3].Quality);
            Assert.AreEqual(2, app.Items[3].SellIn);
        }
    }
}
