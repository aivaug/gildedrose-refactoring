﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.ItemClasses
{
    public class AgedBrieItem : RegularItem
    {
        public AgedBrieItem(Item a) : base(a) { }
        public override int QualityDecrease { get => 1;}
        public override int SellInEndsQualityCof { get => 2; }
    }
}
