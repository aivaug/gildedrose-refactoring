﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.ItemClasses
{
    public class BackstagePassItem : RegularItem
    {
        public BackstagePassItem(Item a) : base(a) { }
        public override int QualityDecrease { get => 1; }
        public override int SellInEndsQualityCof { get => 1; }
        private int QualityValueAfterIncreased1 { get => 2; }
        private int QualityValueAfterIncreased2 { get => 3; }
        public override bool CustomSellInChanges => true;

        public override void ChangeQualityValue()
        {
            if (SellIn == 0) Quality = 0;
            if (Quality > 0)
            {
                if (SellIn > 10) base.ChangeQualityValue();
                if (SellIn <= 10 && SellIn > 5) Quality += QualityValueAfterIncreased1;
                else if (SellIn <= 5) Quality += QualityValueAfterIncreased2;
            }
            SellIn--;
        }
    }
}
