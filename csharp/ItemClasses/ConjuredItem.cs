﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.ItemClasses
{
    public class ConjuredItem : RegularItem
    {
        public ConjuredItem(Item a) : base(a) { }

        public override int QualityDecrease => -2;
        public override int SellInEndsQualityCof => 4;
    }
}
