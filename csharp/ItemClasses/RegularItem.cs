﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.ItemClasses
{
    public class RegularItem : Item
    {
        public RegularItem(Item item)
        {
            Name = item.Name;
            SellIn = item.SellIn;
            Quality = item.Quality;
        }
        public virtual int SellInEndsQualityCof { get => 2; }
        public virtual int QualityDecrease { get => -1; }
        public virtual bool QualityChanges { get => true; }
        public virtual bool SellInChanges { get => true; }
        public virtual bool CustomSellInChanges { get => false; }
        public virtual void ChangeQualityValue()
        {
            if ((Quality > 0 || QualityDecrease>0) && Quality < 50)
            {
                if (SellIn > 0) Quality += QualityDecrease;
                else if (Quality > 1 || QualityDecrease>0) Quality += (QualityDecrease * SellInEndsQualityCof);
                else Quality -= QualityDecrease;
            }
            if(!CustomSellInChanges) SellIn--;
        }
        public override string ToString()
        {
            return string.Format("{0,40} {1,10} {2,-50}", this.Name, this.SellIn, this.Quality);
        }

        public void UpdateItem()
        {
            if (QualityChanges) ChangeQualityValue();
            if (SellInChanges && !CustomSellInChanges && !QualityChanges) SellIn--;
        }
    }
}
