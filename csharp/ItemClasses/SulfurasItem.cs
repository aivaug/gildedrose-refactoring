﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.ItemClasses
{
    public class SulfurasItem : RegularItem
    {
        public SulfurasItem(Item a) : base(a) { }
        public override bool QualityChanges =>false;
        public override bool SellInChanges => false;
    }
}
